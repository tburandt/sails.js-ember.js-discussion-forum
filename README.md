# appulous
### a Sails application


# setting up server
> sudo apt-get update

> sudo apt-get install python-software-properties python g++ make

> sudo add-apt-repository ppa:chris-lea/node.js

> sudo apt-get update

> sudo apt-get install git

> sudo apt-get install nodejs

> sudo npm -g install sails


Here is a cheat sheet for postgres commands:
http://blog.jasonmeridth.com/2012/10/02/postgresql-command-line-cheat-sheet.html


Assign password to the user postgres so we can login with pgAdmin or w/e
http://stackoverflow.com/questions/7695962/postgresql-password-authentication-failed-for-user-postgres
> sudo -u postgres psql -c "ALTER USER postgres PASSWORD 'postgres';"

Notes... This will get us into postgres that we just freshly installed.
> sudo -u postgres psql postgres


Navigate to `/etc/postgresql/8.4/main/pg_hba.conf` and scroll to the bottom
...this guide/answer allows connection with pgAdmin / Navicat
http://stackoverflow.com/questions/3278379/how-to-configure-postgresql-to-accept-all-incoming-connections


# API
Recieving access tokens for client side user. 

/auth -> lists all of the currently authenticated users.

/auth/
/auth/token/create/:internal_id
/auth/token/delete/:internal_id
/auth/key/
From the server send your `secret_key` and the user's `internal_id` to appulous.


We will return an access_token that is unique for the specified internal id. 

