// the Router is where we declare our routes
App.Router.map(function(){

    this.resource('list', { path: "/" }, function() {
        this.route("undefined", { path: "/" });
        this.route("latest", { path: "/latest" });
        this.route("latestCategory", { path: "/category/:category_id" });
    });
    this.resource("topic", { path: "/topic" }, function() {
        this.route("view", { path: "/:topic_id" });
    });
});

App.ApplicationController = Ember.Controller.extend({
    // the initial value of the `search` property
    search: '',

    actions: {
        query: function() {
            // the current value of the text field
            var query = this.get('search');
            this.transitionToRoute('search', { query: query });
        }
    }
});

App.ListUndefinedRoute = Ember.Route.extend({
    beforeModel: function() {
        var lastFilter = this.controllerFor('application').get('lastFilter');
        this.transitionTo('list.latest');
    }
});

// Superclass to be used by all of the filter routes below
App.FilterRoute = Ember.Route.extend({
    activate: function() {
        var controller = this.controllerFor('application');
        console.log("filterRoute");
        controller.set('lastFilter', this.templateName);
    }
});

App.ListLatestRoute = App.FilterRoute.extend({

    model: function(params) {
        return [];
    },

    setupController: function(controller, model) {

        return App.Topic.fetch().then(function(records) {

            console.log(records);
            controller.set("content", records);

        }, function(error) {
            console.log(error);
        });
    }
});

App.ListLatestCategoryRoute = App.FilterRoute.extend({

    model: function(params) {
        this.set('params', params);
        return [];
    },

    setupController: function(controller, model) {

        console.log(this.get('params'));

        App.Topic.fetch({

            categoryId: this.get('params').category_id

        }).then(function(records) {

            console.log(records);
            controller.set("content", records);

        }, function(error) {
            console.log(error);
        });

    }
});

App.ListLatestCategoryController = Ember.ArrayController.extend();


App.ListLatestCategoryIndexRoute = Ember.Route.extend({
    model: function() {
        return [];
    }
});

App.TopicViewRoute = Ember.Route.extend({

    model: function(params) {
        return { id: params.topic_id };
    },

    setupController: function(controller, model) {

        App.Topic.fetch({ id: model.id }).then(function(records) {

            console.log(records);
            controller.set("content", records);
            this._super();

        }, function(error) {
            console.log(error);
        });
    }
});

App.NavigationView = Ember.View.extend({
    // We are setting templateName manually here to the default value
    templateName: "navigation",
    test: 'test',
    model: function() {
        return App.Category.find();
    }.property()
});

