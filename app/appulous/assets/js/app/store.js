

App.RESTAdapter = RL.RESTAdapter.create({
    url: '//192.168.50.4:1337'
    ,namespace: 'api'
});

App.Client = RL.Client.create({
    adapter: App.RESTAdapter
});

App.RESTAdapter.configure("plurals", {
    forum_topic: "forumTopic"
    ,forum_category: "forumCategory"
});



// models

/**
 * api/topic
 */
App.Topic = RL.Model.extend({
    id: RL.attr('string')
    ,title: RL.attr('string')
    ,body: RL.attr('string')
    ,viewCount: RL.attr('number')
    ,replyCount: RL.attr('number')
    ,createdAt: RL.attr('string')
    ,updatedAt: RL.attr('string')

    // relations
    ,category: RL.belongsTo('App.Category')
//    ,posts: RL.hasMany(App.Post)
});

App.Topic.reopenClass({
    resourceName: 'forum_topic'
});


/**
 * api/category
 */
App.Category = RL.Model.extend({
    id: RL.attr('string')
    ,name: RL.attr('string')
//    isPublished: RL.attr('boolean'),
//    readCount:   RL.attr('number'),
//    createdAt:   RL.attr('date')

    // relations
    ,topics: RL.hasMany('App.Topic')
});

App.Category.reopenClass({
    resourceName: 'forum_category'
});

