/**
 * User
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */
module.exports = {

    autoPk: false,

    attributes: {

        id: {
            type: 'string',
            unique: true
        },

        username: 'string',
        email: {
            type: 'string'
        },
        avatar: {
            type: 'string'
        },
        firstName: 'string',
        lastName: 'string',
        password: {
            type: 'string'
        },

        // somehow not having this throws error
        // but this needs to be removed
        tenantId: {
            type: 'string',
            index: true
        },
        // Timestamp is not supported but Time, Date, and DateTime are
        updatedAt: 'datetime',
        createdAt: 'datetime',

        // Override toJSON instance method
        // to remove password value
        toJSON: function() {
            var obj = this.toObject();
            delete obj.password;
            return obj;
        }
    },

    /**
     * Lifecycle Callbacks
     *
     * Run before and after various stages:
     *
     * beforeValidation
     * afterValidation
     * beforeUpdate
     * afterUpdate
     * beforeCreate
     * afterCreate
     * beforeDestroy
     * afterDestroy
     */
    beforeCreate: function(values, cb) {

        values.id = 'acc_' + random(14);

        // password hashing
//        if (!values.pass_first || values.pass_first != values.pass_repeat) {
//            return cb("Password doesn't match password confirmation.");
//        }

        require('bcrypt').hash(values.password, 10, function(err, hash) {
            if (err) return cb(err);
            values.password = hash;
            cb();
        });
    },

    beforeUpdate: function (values, cb) {

        cb();
    }
};

function random(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < length; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}