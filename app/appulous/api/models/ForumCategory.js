/**
 * ForumCategory
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    autoPk: false,

    attributes: {

        id: {
            type: 'string',
            unique: true
        },
        name: {
            type: 'string'
        },
        tenantId: {
            type: 'string',
            index: true
        },


        // Override toJSON instance method
        // to remove password value
        toJSON: function() {
            var obj = this.toObject();
            delete obj.tenantId;
            return obj;
        }
    },

    /**
     * Lifecycle Callbacks
     *
     * Run before and after various stages:
     *
     * beforeValidation
     * afterValidation
     * beforeUpdate
     * afterUpdate
     * beforeCreate
     * afterCreate
     * beforeDestroy
     * afterDestroy
     */
    beforeCreate: function(values, cb) {

        values.id = 'cat_' + random(14);

        cb();
    },
//
    beforeUpdate: function (values, cb) {

        cb();
    }

};

function random(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < length; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}
