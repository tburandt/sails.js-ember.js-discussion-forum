/**
 * Tenant
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */
module.exports = {

	autoPk: false,

    attributes: {
  	    
    	id: {
    		type: 'string',
    		unique: true,
    		empty: true
    	},

        accountId: {
            type: 'string',
            required: true
        },

  	    secretKey: {
  	    	type: 'string',
  	    	unique: true,
    		empty: true
  	    },

  	    publicKey: {
  	    	type: 'string',
  	    	unique: true
  	    },

        webAddress: {
            type: 'string'
        },

        subDomain: {
            type: 'string',
            unique: true
        },

		// Override toJSON instance method
		// to remove password value
		toJSON: function() {
			var obj = this.toObject();
			delete obj.secretKey;
			return obj;
		}
    },

	/**
	* Lifecycle Callbacks
	*
	* Run before and after various stages:
	*
	* beforeValidation
	* afterValidation
	* beforeUpdate
	* afterUpdate
	* beforeCreate
	* afterCreate
	* beforeDestroy
	* afterDestroy
	*/
	beforeCreate: function(values, cb) {

		values.id = 'id_' + random(14);
		values.secretKey = 'sk_' + random(61);
		values.publicKey = 'pk_' + random(61);

		cb();

		// // an example encrypt function defined somewhere
		// encrypt(values.password, function(err, password) {
		// 	if(err) return cb(err);

		// 	values.password = password;
		// 	cb();
		// });
	},

	beforeUpdate: function (values, cb) {

		cb();
	}

};

function random(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < length; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}