/**
 * AccountController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

module.exports = {
    
  
	// POST https://api.harvestapp.com/oauth2/token
	// {
	// 	"code":          "[authorization code from Harvest]",
	// 	"client_id":     "[your application's client ID]",
	// 	"client_secret": "[your application's client secret]",
	// 	"redirect_uri":  "[your application's redirect URI]",
	// 	"grant_type":    "authorization_code"
	// }
	test: function (req, res) {

		var token = random(64);
		res.json({ access_token: token });
	},

	/**
	* Overrides for the settings in `config/controllers.js`
	* (specific to HomeController)
	*/
	_config: {}

};

function random(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789%/+";

    for( var i=0; i < length; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}