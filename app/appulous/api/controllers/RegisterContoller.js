/**
 * TenantController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

module.exports = {


    // POST https://api.harvestapp.com/oauth2/token
    // {
    // 	"code":          "[authorization code from Harvest]",
    // 	"client_id":     "[your application's client ID]",
    // 	"client_secret": "[your application's client secret]",
    // 	"redirect_uri":  "[your application's redirect URI]",
    // 	"grant_type":    "authorization_code"
    // }
    account: function (req, res) {



        // find the tenant
        Tenant.findOne({

            public_key: req.body.public_key

        }).done(function(err, tenant) {

            // Error handling
            if (err) return res.forbidden(err);
            if (!tenant) return res.forbidden("Invalid 'public_key'.");

            // find the tenant assigned to this user
            User.findOne({

                access_token: req.body.access_token,
                tenant_id: tenant.id

            }).done(function(err, user) {

                // Error handling
                if (err) return res.forbidden(err);
                if (!User) return res.forbidden("Invalid 'access_token'.");

                req.session.user_id = user.id;
                req.session.tenant_id = tenant.id;
                req.session.access_token = user.access_token;
                req.session.authenticated = true;
                console.log(req.session);
                res.json(req.session);
            });

        });
    },

    /**
     * Overrides for the settings in `config/controllers.js`
     * (specific to UserController)
     */
    _config: {

        blueprints: {
            prefix: '/api'
        }

    }


};
