// policies/canWrite.js
module.exports = function(req, res, next) {

    var accessToken = req.session.user.accessToken;

    User.findOne({ accessToken: accessToken }, function(err, user) {

        if (err) {

            return res.forbidden('You are not permitted to perform this action.');

        } else {

            return next();

        }
    });
};